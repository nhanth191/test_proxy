﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DownloadViaProxy.Models
{
    public class DownloadItem
    {
        public string Url { get; set; }
        public string Name { get; set; }
        public DownloadStatus Status { get; set; }
        public string IP { get; set; }
    }

    public enum DownloadStatus
    {
        Downloading,
        Success,
        Fail
    }
}
