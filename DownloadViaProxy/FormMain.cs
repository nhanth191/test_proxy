﻿using DownloadViaProxy.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DownloadViaProxy
{
    public partial class FormMain : Form
    {
        static WebProxy proxy;
        
        public FormMain()
        {
            InitializeComponent();
            
            proxy = new WebProxy("zproxy.lum-superproxy.io", 22225);
            proxy.Credentials = new NetworkCredential("lum-customer-hl_cc67f9d6-zone-static", "vn123456789");
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            var html = UseWebRequest("", true);

            var regex = new Regex("<a class=\"btn btn-primary dlButton addDownloadedBook\" href=\"([a-zA-Z0-9\\/]{0,})\"", RegexOptions.Multiline);
            var match = regex.Match(html);
            if (match.Success)
            {
                var link = "https://1lib.eu" + match.Groups[1];
                var result = UseWebRequest(link, false);

                //http://dl101.zlibcdn.com/dtoken/710986ddbd71f875f172b285b99197f3
                var linkRegex = new Regex(".+\\/dtoken\\/.+");
                while (!linkRegex.Match(result).Success)
                {
                    result = UseWebRequest(result, false);
                }



                UseWebClient(result);
            }
        }
        static List<DownloadItem> ReadCSV(string csvFile)
        {
            var lines = File.ReadAllLines(csvFile);

            return null;
        }
        static string UseWebRequest(string _url, bool redirect)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(_url);
                //request.CookieContainer = SessionCookieContainer;
                request.Proxy = proxy;
                request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; rv:11.0) like Gecko";
                request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";
                request.AllowAutoRedirect = redirect;

                // Send the 'HttpWebRequest' and wait for response.
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();


                if (redirect == true)
                {
                    string result = "";

                    System.IO.Stream stream = response.GetResponseStream();
                    System.Text.Encoding ec = System.Text.Encoding.GetEncoding("utf-8");
                    System.IO.StreamReader reader = new System.IO.StreamReader(stream, ec);
                    char[] chars = new Char[256];
                    int count = reader.Read(chars, 0, 256);
                    while (count > 0)
                    {
                        string str = new String(chars, 0, 256);
                        result = result + str;
                        count = reader.Read(chars, 0, 256);
                    }
                    response.Close();
                    stream.Close();
                    reader.Close();
                    return result;
                }
                else
                {
                    return response.Headers.Get("Location");
                }

            }
            catch (Exception exp)
            {
                string str = exp.Message;
                return null;
            }
        }

        static void UseWebClient(string _url)
        {
            WebClient client = new WebClient();

            //client.Headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
            //client.Headers.Add("Accept-Encoding", "gzip, deflate, br");
            //client.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36 Edg/84.0.522.59");
            //client.Headers.Add("Accept-Charset", "ISO-8859-1");
            client.Proxy = proxy;

            client.DownloadFileCompleted += new AsyncCompletedEventHandler((s, e) => {
                Console.WriteLine("DONE");
            });
            client.DownloadProgressChanged += new DownloadProgressChangedEventHandler((s, e) => {
                Console.WriteLine(e.ProgressPercentage);
            });

            client.DownloadFileAsync(new Uri(_url), "test.html");
        }

        private static async void Run()
        {
            var response = await GetResponse("http://www.livescore.com/").ConfigureAwait(false);

            Console.ReadLine();
            // or var response = GetResponse("http://www.livescore.com/").Result;
        }

        private static async Task<string> GetResponse(string url)
        {
            try
            {
                HttpClient _HttpClient = new HttpClient();
                using (var request = new HttpRequestMessage(HttpMethod.Get, new Uri(url)))
                {
                    request.Headers.TryAddWithoutValidation("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
                    request.Headers.TryAddWithoutValidation("Accept-Encoding", "gzip, deflate, br");
                    request.Headers.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36 Edg/84.0.522.59");
                    request.Headers.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1");

                    using (var response = await _HttpClient.SendAsync(request).ConfigureAwait(false))
                    {
                        response.EnsureSuccessStatusCode();
                        using (var responseStream = await response.Content.ReadAsStreamAsync().ConfigureAwait(false))
                        using (var decompressedStream = new GZipStream(responseStream, CompressionMode.Decompress))
                        using (var streamReader = new StreamReader(decompressedStream))
                        {
                            return await streamReader.ReadToEndAsync().ConfigureAwait(false);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine();
                return null;
            }
        }
    }
}
